/*

Copyright 2020 fuwa

This file is part of Wowllet.

Wowllet is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wowllet is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wowllet.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dart:math';
import 'dart:core';
import 'dart:convert';

import 'package:crclib/crclib.dart';

import 'mnemonics/english.dart' as mnemonics;

final rand = Random.secure();

const int seedLength = 24;

List<int> asDigits(final BigInt x, final int base) => asDigits1(x, base, []);

List<int> asDigits1(final BigInt x, final int base, final List<int> digits) {
  if (x < BigInt.from(base)) {
    return [x.toInt(), ...digits];
  } else {
    final BigInt q = x ~/ BigInt.from(base);
    final int r = x.remainder(q).toInt();
    return asDigits1(q, base, [r, ...digits]);
  }
}

int hashAsIndexOf(final List<String> words) {
  final String sig =
      words.map((x) => x.substring(0, mnemonics.prefixLength)).join();
  final int hash = Crc32Zlib().convert(utf8.encode(sig));
  return hash % seedLength;
}

List<String> randomSeed() {
  final int length = mnemonics.words.length;
  int next() => rand.nextInt(length);

  final List<String> words =
      Iterable.generate(seedLength, (i) => mnemonics.words[next()]).toList();

  return [...words, words[hashAsIndexOf(words)]];
}

bool isValid(final List<String> tokens) {
  final validTokens =
      tokens.map(mnemonics.words.contains).fold(true, (x, y) => x && y);

  if (tokens.length == 25 && validTokens) {
    final subTokens = tokens.take(24).toList();
    final hashIdx = hashAsIndexOf(subTokens);
    return tokens[24] == subTokens[hashIdx];
  } else {
    // hack to deal with non-english
    return tokens.length == 25;
  }
}

List<String> tidy(final String input) {
  final List<String> tokens = input.trim().split(RegExp(r"\s+"));
  final _tokens = tokens.reversed.take(25).toList().reversed.toList();
  return _tokens;
}
